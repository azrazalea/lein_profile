{:user {:plugins [[cider/cider-nrepl "0.10.0-SNAPSHOT"]
                  [jonase/eastwood "0.2.1"]
                  [lein-bikeshed "0.2.0"]
                  [lein-kibit "0.1.2"]
                  [lein-ancient "0.6.7"]
                  [lein-cljfmt "0.3.0"]]
        :dependencies [[org.clojure/tools.nrepl "0.2.10"]]
        :aliases {"analyze" [["do"]
                             ["ancient"]
                             ["with-profile" "production" "deps" ":tree"]
                             ["kibit"]
                             ["bikeshed" "-m" "120"]
                             ["eastwood" "{:namespaces [:source-paths]}"]
                             ["check"]]}}}
